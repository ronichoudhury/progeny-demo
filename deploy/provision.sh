user=$(logname)

export PATH=/home/${user}/demodock/bin:$PATH

cd /home/${user}
if [ ! -e progeny-demo ]; then
    git clone https://gitlab.kitware.com/ronichoudhury/progeny-demo.git
fi

cd progeny-demo/deploy
demodock build progeny
demodock start progeny progeny
demodock update >/home/${user}/demoweb/src/demos.json
