/*jshint browser: true, jquery: true */
/*global clique, d3, tangelo, _, Backbone, Slider, jsyaml */

$(function () {
    "use strict";

    $.get("config.yaml").then(function (text) {
        var zoomLevel = 6,
            config,
            adjustZoom,
            graph,
            cmap,
            colorblindCmap,
            regularCmap,
            setSize,
            view,
            info,
            linkInfo,
            mixer = 1.0,
            getAllNeighborNodes,
            args,
            idReq;

        config = jsyaml.load(text);

        // Hide the distance slider if not needed.
        if (!config.distanceMixer) {
            d3.select("#distanceslider")
                .classed("hidden", true);
        }

        // Construct a colormap scale from a colorblind-safe colormap
        // (http://colorbrewer2.org/?type=qualitative&scheme=Paired&n=4).
        colorblindCmap = d3.scale.ordinal().range(["#a6cee3","#1f78b4","#b2df8a","#33a02c"]);

        // Construct a "regular" colormap scale as well.
        regularCmap = d3.scale.category10();

        // By default, the colormap will be the "regular" one.
        cmap = regularCmap;

        adjustZoom = function (dir) {
            if (d3.select(this).classed("disabled")) {
                return;
            }

            zoomLevel += dir;

            d3.select("#zoom-val")
                .text(6 - zoomLevel);

            d3.select("#zoom-in")
                .classed("disabled", zoomLevel === 0);

            d3.select("#zoom-in > a")
                .attr("disabled", zoomLevel === 0 ? true : null);

            d3.select("#zoom-out")
                .classed("disabled", zoomLevel === 6);

            d3.select("#zoom-out > a")
                .attr("disabled", zoomLevel === 6 ? true : null);

            Backbone.Events.trigger("zoomLevelChanged");
        };

        $("#zoom-out").on("click", _.partial(adjustZoom, 1));
        $("#zoom-in").on("click", _.partial(adjustZoom, -1));

        window.graph = graph = new clique.Graph({
            adapter: new tangelo.plugin.mongo.Mongo({
                host: config.mongo.host,
                database: config.mongo.database,
                collection: config.mongo.collection
            })
        });

        setSize = function (sel, width, height, padding) {
            var panelWidth = width + 2 * padding,
                panelHeight = height + 2 * padding;

            sel.selectAll("rect")
                .transition()
                .duration(300)
                .attr("x", -panelWidth / 2)
                .attr("y", -panelHeight / 2)
                .attr("width", panelWidth)
                .attr("height", panelHeight)
                .attr("rx", padding)
                .attr("ry", padding);

            sel.selectAll("image")
                .transition()
                .duration(300)
                .attr("x", -width / 2)
                .attr("y", -height / 2)
                .attr("width", width)
                .attr("height", height);
        };

        window.view = view = new clique.view.Cola({
            model: graph,
            el: "#content",
            linkDistance: function (d) {
                var level,
                    data;

                level = function (distance, startLevel) {
                    var level,
                        threshold,
                        excess;

                    threshold = [100, 200, 300, 400, 500, 700, Infinity];

                    // This executes the callback for each element in the array
                    // until a falsy value is returned.
                    _.every(threshold, function (t, i) {
                        if (distance < t) {
                            level = i;
                            excess = distance - (threshold[i - 1] || 0);
                            return false;
                        }

                        return true;
                    });

                    // Adjust the level downward to compensate for semantic zoom
                    // level.
                    level = level - startLevel;
                    if (level < 0) {
                        level = 0;
                    }

                    // Similarly adjust the "apparent distance" such a zoom level
                    // requires.
                    distance = (threshold[level - 1] || 0) + excess;

                    return {
                        level: level,
                        distance: distance
                    };
                };

                data = level(config.mixer(mixer, d.data.distance[0], d.data.distance[1]), zoomLevel);

                return 100 + data.level * 125 + data.distance;
            },
            nodeEnter: function (enter) {
                enter.append("rect")
                    .style("stroke", "rgb(0,0,0)")
                    .style("stroke-width", "1px");

                enter.append("image")
                    .attr("xlink:href", config.xlinkHref);

                enter.call(setSize, config.size.width, config.size.height, config.size.padding);
            },
            renderNodes: function (nodes) {
                var focused = this.selection.focused(),
                    focusTag = focused && graph.nodes[focused] && graph.nodes[focused].data.tag,
                    linkFocused;

                nodes.datum(function (d) {
                    d.matches = d.data.tag === focusTag;
                    return d;
                });

                _.delay(function () {
                    nodes.selectAll("rect")
                        .style("fill", config.rectFill(cmap))
                        .transition()
                        .duration(300)
                        .style("stroke", function (d) {
                            return d.matches ? "rgb(255,0,0)" : "rgb(0,0,0)";
                        })
                        .style("stroke-width", function (d) {
                            return d.matches ? "3px" : "1px";
                        });
                }, 301);

                nodes.call(setSize, config.size.width, config.size.height, config.size.padding)
                    .filter(function (d) {
                        return d.key === focused;
                    })
                    .call(setSize, 2 * config.size.width, 2 * config.size.height, 2 * config.size.padding);

                linkFocused = function (d) {
                    return d.source.key === focused || d.target.key === focused;
                };

                this.links.selectAll("path:not(.handle)")
                    .transition()
                    .duration(700)
                    .style("fill", function (d) {
                        return linkFocused(d) ? "rgb(0,0,0)" : "rgb(119,136,143)";
                    })
                    .style("opacity", function (d) {
                        return focused ? (linkFocused(d) ? 1.0 : 0.3) : 0.7;
                    });
            }
        });

        Backbone.Events.on("zoomLevelChanged sliderChanged", view.cola.start, view.cola);

        view.addLink = _.bind(function (key) {
            graph.adapter.findLinkByKey(key).then(function (link) {
                if (!link) {
                    console.warn("link with key " + key + " does not exist");
                    return;
                }

                $.when(graph.adapter.findNodeByKey(link.source()), graph.adapter.findNodeByKey(link.target()))
                    .then(function (source, target) {
                        graph.addNode(source);
                        graph.addNode(target);
                    });
            });
        }, view);

        getAllNeighborNodes = function (adapter, args) {
            return $.getJSON("assets/getNeighbors", _.extend(args, adapter.mongoStore)).then(function (data) {
                return {
                    nodes: _.map(data.nodes, adapter.addAccessor, adapter),
                    links: data.links
                };
            });
        };

        view.on("render", function () {
            var $cm,
                getMenuPosition;

            $cm = $("#contextmenu");

            // This returns a position near the mouse pointer, unless it is too
            // near the right or bottom edge of the window, in which case it
            // returns a position far enough inside the window to display the
            // menu in question.
            getMenuPosition = function (mouse, direction, scrollDir) {
                var win = $(window)[direction](),
                    scroll = $(window)[scrollDir](),
                    menu = $("#contextmenu")[direction](),
                    position = mouse + scroll;

                if (mouse + menu > win && menu < mouse) {
                    position -= menu;
                }

                return position;
            };

            // Attach a contextmenu action to all the nodes - it populates the
            // menu element with appropriate data, then shows it at the
            // appropriate position.
            d3.select(view.el)
                .selectAll("g.node")
                .on("contextmenu", function (d) {
                    var cm = d3.select("#contextmenu"),
                        ul = cm.select("ul"),
                        node = graph.adapter.getAccessor(d.key),
                        report,
                        left,
                        top;

                    left = getMenuPosition(d3.event.clientX, "width", "scrollLeft");
                    top = getMenuPosition(d3.event.clientY, "height", "scrollTop");

                    report = config.report(d);

                    cm.select("ul")
                        .select("li.nodelabel")
                        .html(report);

                    ul.select("a.context-open")
                        .on("click", function () {
                            window.open(d.data.image, "_blank");
                        });

                    ul.select("a.context-hide-others")
                        .on("click", function () {
                            _.each(_.keys(graph.nodes), function (key) {
                                var accessor;

                                if (key !== node.key()) {
                                    accessor = graph.adapter.getAccessor(key);

                                    accessor.setAttribute("selected", false);
                                    accessor.clearAttribute("root");
                                    graph.removeNode(accessor);
                                }
                            });
                        });

                    ul.select("a.context-hide")
                        .on("click", function () {
                            node.setAttribute("selected", false);
                            node.clearAttribute("root");
                            graph.removeNode(node);
                        });

                    ul.select("a.context-expand").on("click", function () {
                        getAllNeighborNodes(graph.adapter, {
                            origin: node.key(),
                            threshold: 100000,
                            nodes: JSON.stringify(_.keys(graph.nodes))
                        }).then(function (data) {
                            _.each(data.nodes, function (node) {
                                graph.addNode(node, _.map(data.links[node.key()], graph.adapter.addAccessor, graph.adapter));
                            });
                        });
                    });

                    ul.select("a.context-collapse")
                        .on("click", _.bind(clique.view.SelectionInfo.collapseNode, info, node));

                    $cm.show()
                        .css({
                            left: left,
                            top: top
                        });

                    return false;
                });

            // Clicking anywhere else will close any open context menu.  Use the
            // mouseup event (bound to only the left mouse button) to ensure the
            // menu disappears even on a selection event (which does not
            // generate a click event).
            d3.select(document.body)
                .on("mouseup.menuhide", function () {
                    if (d3.event.which !== 1) {
                        return;
                    }
                    $cm.hide();
                });
        });

        // Show a screencap in a special area whenever a node is focused.
        view.selection.on("focused", function (key) {
            var cap = d3.select("#screencap");

            if (!key) {
                cap.selectAll("img")
                    .transition()
                    .duration(700)
                    .style("opacity", 0)
                    .remove();
                return;
            }

            cap.append("img")
                .attr("src", graph.nodes[key].data.image)
                .attr("width", 380)
                .style("position", "absolute")
                .style("opacity", 0)
                .transition()
                .duration(700)
                .style("opacity", 1);
        });

        linkInfo = new clique.view.LinkInfo({
            model: view.linkSelection,
            el: "#link-info",
            graph: graph
        });
        linkInfo.render();

        $("#zoom-val").text(6 - zoomLevel);

        d3.select("#show-zoom")
            .on("change", function () {
                d3.select("#zoom-control")
                    .style("display", this.checked ? null : "none");
            });

        d3.select("#show-link")
            .on("change", function () {
                d3.select("#link")
                    .style("display", this.checked ? null : "none");
            });

        d3.select("#show-screencap")
            .on("change", function () {
                d3.select("#screencap")
                    .style("display", this.checked ? null : "none");
            });

        d3.select("#colorblind")
            .on("change", function () {
                cmap = this.checked ? colorblindCmap : regularCmap;
                view.render();
            });

        var slider = new Slider("#slider", {
            tooltip: "hide"
        });
        slider.on("slide", _.debounce(function (value) {
            mixer = value;
            Backbone.Events.trigger("sliderChanged");
        }, 500));

        // Check for initialization arguments.
        args = tangelo.queryArguments();
        if (_.size(args) > 0) {
            if (_.has(args, "colorblind")) {
                // Construct a colormap scale from a colorblind-safe colormap
                // (http://colorbrewer2.org/?type=qualitative&scheme=Paired&n=4).
                cmap = d3.scale.ordinal().range(["#a6cee3","#1f78b4","#b2df8a","#33a02c"]);
            }

            // If an id is supplied in the query args, use that to bootstrap the
            // application.
            if (_.has(args, "id")) {
                idReq = graph.adapter.findNode({id: args.id});
            } else if (_.has(args, "smqtk_id")) {
                idReq = graph.adapter.findNode({smqtk_id: args.smqtk_id});
            }
        }

        // If there's no id specified in the query args, but there is one in the
        // config file, use that one.
        if (!idReq && config.initial) {
            idReq = graph.adapter.findNode({id: String(config.initial)});
        }

        if (idReq) {
            idReq.then(function (node) {
                if (node) {
                    graph.addNode(node);
                } else {
                    console.warn("No such node with id " + args.id);
                }
            });
        }
    });
});
