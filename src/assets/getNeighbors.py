from pymongo import MongoClient
from bson.objectid import ObjectId
import json


def process_link(link):
    link["key"] = str(link["_id"])
    del link["_id"]
    link["source"] = str(link["source"])
    link["target"] = str(link["target"])

    return link


def run(host=None, db=None, coll=None, origin=None, nodes=None, threshold=None):
    # Interpret arguments.
    nodes = json.loads(nodes)
    threshold = float(threshold)

    # Connect to Mongo.
    client = MongoClient(host)
    database = client[db]
    graph = database[coll]

    # Find all links originating or terminating at "origin".
    oid = ObjectId(origin)
    links = graph.find({"type": "link",
                        "$or": [{"source": oid},
                                {"target": oid}],
                        "data.distance": {"$lt": threshold}})

    # Collect all the node keys lying on the other end of these links.
    oids = []
    for link in links:
        source = link["source"]
        which = "target" if source == oid else "source"
        oids.append(link[which])

    # Find the records for these keys.
    data = [graph.find_one({"_id": o}) for o in oids]

    # Process the data.
    for d in data:
        d["key"] = str(d["_id"])
        del d["_id"]

    # Create a master list of all existing nodes (the ones originally in the
    # graph, and the new neighbor nodes).
    all_nodes = map(ObjectId, nodes + oids)

    # For each new neighbor node, find all links to nodes in the master list.
    link_table = {}
    for node in map(ObjectId, oids):
        links = graph.find({"type": "link",
                            "$or": [{"source": node,
                                     "target": {"$in": all_nodes}},
                                    {"target": node,
                                     "source": {"$in": all_nodes}}]})
        link_table[str(node)] = map(process_link, links)

    return {"nodes": data,
            "links": link_table}
