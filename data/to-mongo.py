from bson.objectid import ObjectId
import bson.json_util
import csv
import os
import sys
import yaml


def process_key_file(filename, metaIndex):
    table = {}

    with open(filename) as f:
        rows = (line.split() for line in f.readlines())

    # Each row consists of whitespace-separated fields:  image filename and then
    # ID number.
    for i, (image, photo_id, smqtk_id) in enumerate(rows):
        photo_id = photo_id
        oid = ObjectId()

        meta = os.path.basename(image).split("_")
        show = meta[0]
        label = meta[metaIndex]
        tag = "(%s,%s)" % (show, label)

        print bson.json_util.dumps({"_id": oid,
                                    "type": "node",
                                    "data": {"id": photo_id,
                                             "smqtk_id": smqtk_id,
                                             "tag": tag,
                                             "image": image}})

        table[photo_id] = oid

        if (i + 1) % 501 == 0:
            print >>sys.stderr, "%d records processed" % (i)

    return table


def sort(x, y):
    return (x, y) if x <= y else (y, x)


def get_csv_dict(filename, nodes, table):
    basename = os.path.basename(filename)

    mini = float("inf")
    maxi = -float("inf")

    with open(filename) as f:
        basename = os.path.splitext(basename)[0]
        for i, (img1, img2, distance) in enumerate(csv.reader(f)):
            s = sort(img1, img2)
            distance = float(distance)

            if distance < mini:
                mini = distance

            if distance > maxi:
                maxi = distance

            oid = ObjectId()

            rec = table.setdefault(s, {"_id": oid,
                                       "type": "link",
                                       "source": nodes[s[0]],
                                       "target": nodes[s[1]],
                                       "data": {"distance": [],
                                                "filename": basename}})

            rec["data"]["distance"].append(distance)

            if (i + 1) % 501 == 0:
                print >>sys.stderr, "%d records processed" % (i)

        return (mini, maxi, table)


def main():
    if len(sys.argv) < 2:
        print >>sys.stderr, "usage: to-mongo.py <configfile> <data-dir>"
        return 1

    configfile = sys.argv[1]
    datadir = sys.argv[2]

    # Read in config file and extract the metaIndex property.
    class IgnoreJavascript(yaml.Loader):
        pass
    IgnoreJavascript.add_constructor("!!js/function", lambda x, y: None)
    with open(configfile) as f:
        config = yaml.load(f.read(), IgnoreJavascript)
    metaIndex = config["metaIndex"]

    # Open directory and find key file and any data files.
    keyfile = None
    csvfiles = []
    for f in os.listdir(datadir):
        if f == "key_file.txt":
            keyfile = os.path.join(datadir, f)
        elif f.endswith(".csv"):
            csvfiles.append(os.path.join(datadir, f))

    print >>sys.stderr, "Processing key file %s" % (keyfile)
    table = process_key_file(keyfile, metaIndex)

    global_min = float("inf")
    global_max = -float("inf")

    data = {}
    for csvfile in csvfiles:
        print >>sys.stderr, "Processing data file %s" % (csvfile)
        # mini, maxi = process_csv_file(csvfile, table, seen)
        mini, maxi, data = get_csv_dict(csvfile, table, data)

        print bson.json_util.dumps({"_id": ObjectId(),
                                    "minmax": os.path.splitext(os.path.basename(csvfile))[0],
                                    "min": mini,
                                    "max": maxi})

        if mini < global_min:
            global_min = mini

        if maxi > global_max:
            global_max = maxi

    print bson.json_util.dumps({"_id": ObjectId(),
                                "minmax": "global",
                                "min": global_min,
                                "max": global_max})

    for i, (_, rec) in enumerate(data.iteritems()):
        print bson.json_util.dumps(rec)
        if (i + 1) % 501 == 0:
            print >>sys.stderr, "%d records written" % (i)


if __name__ == "__main__":
    sys.exit(main())
