# progeny-demo
Progeny demo using Clique

## Build Instructions

1. Make sure the following software is installed:
    - Git
    - MongoDB
    - NPM (Node Package Manager)
    - Python (2.7)

2. Install Python requirements.
````bash
sudo pip install pymongo tangelo
````

3. Install gulp.
````bash
sudo npm install -g gulp
````

4. Clone the repository.
````bash
git clone https://gitlab.kitware.com/ronichoudhury/progeny-demo.git
cd progeny-demo
````

5. Install the Node requirements.
````bash
npm install
````

6. Build the application.
````bash
gulp
````

7. Prepare the Mongo database.
````bash
python data/to-mongo.py <data-directory> | mongoimport -d progeny -c demo --drop
````

8. Create Mongo indexes on the "source" and "target" fields.
````bash
mongo
> use progeny
> db.demo.ensureIndex({source:1})
> db.demo.ensureIndex({target:1})
> exit
````

8. Link the image data into the application.
````bash
cd build/site
ln -s <data-directory>/images
````

9. Launch the application.
````bash
cd ../..
tangelo --port 8080 --root build/site --config tangelo-config.yaml
````

10. Use the application.
````bash
chrome http://localhost:8080/?id=2838
````
